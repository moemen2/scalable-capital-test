## Task 1 - Clean up, refactor and write tests

1. grouping routes in a folder -> organization
1. grouping components in a folder -> organization
1. grouping util files in a folder (helpers) -> organization
1. matching file names with class/component naming convention
1. Routes lazy loading instead of static loading -> performance
1. Adding eslint -> format and style 
1. Moving all default prop values to a global config file -> organization, consistency, and less redundancy
1. Fixing a bug of default prop set to 3 in chart and table components -> bug fix
1. fixing general declaration issues by using "let" or "const" instead of "var" -> format and style
1. adding a missing propTypes and defaultProps in chart component -> to avoid errors if no props passed to component 
1. Separating business logic from ui in table component -> format and style
1. Rounding to last 2 decimals -> UI/UX
1. Adding tests

## Task 2 - Integrate an API

1. adding Axios HTTP client  
1. creating a ConesService file for API


## Task 3 - Add a new feature

1. adding general Input component
1. implementing Initial Investment Sum input using Input component
1. implementing Years of investment input using Input component
1. implementing Monthly Investment Sum input using Input component

## Todo

1. extending test coverage
1. creating data models