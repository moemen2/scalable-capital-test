# Change Log

  #### Please check all the changes done in the [change log](./CHANGELOG.md)
  

# Frontend Developer Test Assignment

We prepared a simple React App which displays multiple performance projection time series in a table as well as in a chart. You can download the project from https://drive.google.com/open?id=1PxsoK7WoPnb4FKJ2-bWRbycDfnk_65oJ. 

The following tasks will require you to modify and extend this app in different ways. You are allowed to add dependencies and change whatever you like as long as you stay with React.

We accept incomplete solutions - but let us know what’s missing. Don’t spend more than 3 hours on the tasks

## Prerequisites
Run the following commands to install the necessary dependencies and start the app:

npm install

npm start

This will start a webpack DevServer which allows you to access the app under http://localhost:3000.


## Task 1 - Clean up, refactor and write tests

Clean up the project, refactor the code and make it prettier. Test it to ensure it works (as before)!
Document your steps and describe why you decided for a certain change.

## Task 2 - Integrate an API

The app contains a cones.json with data which is required for the calculation of the time series
(it describes the average yearly return (mu) and its standard deviation (sigma) for each risk level).
In a real-world scenario, this data could change over time. So it is better to load it from an API
instead of having it in the code base. The DevServer is configured in a way that it provides the cones
under http://localhost:3000/api/cones.

Request the data via HTTP from this URL instead of importing the JSON directly.

## Task 3 - Add a new feature

The product management decided it would be nice if the user of the app could change the initial
investment sum and not just the risk level.

Implement this feature in any way you like and - if you still have some time left - think about other
parameters which could become changeable as well.
