import React from 'react';
import RiskLevelSelector from './RiskLevelSelector';

import Enzyme, { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });


it('renders correctly', () => {
    const wrapper = shallow(<RiskLevelSelector />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

it('should call defaultRiskLevel prop', () => {
    const defaultRiskLevelMock = jest.fn();
    const event = {
        preventDefault() { },
        target: { value: 10 }
    };
    const component = shallow(<RiskLevelSelector onChangeRiskLevel={defaultRiskLevelMock} />);
    component.find('select').simulate('change', event);
    expect(defaultRiskLevelMock).toBeCalledWith(10);
});


