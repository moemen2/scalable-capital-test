import React from 'react';
import Menu from './Menu';
import {render} from '@testing-library/react';
import { BrowserRouter as Router } from "react-router-dom";



it('renders with table and chart links', () => {
    const { getByText } = render(<Router><Menu /></Router>);

    expect(getByText(/table/i).textContent).toBe("Table");
    expect(getByText(/chart/i).textContent).toBe("Chart");

 });


