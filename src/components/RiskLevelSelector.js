import React from 'react';
import PropTypes from 'prop-types';
import { config } from '../config';

class RiskLevelSelector extends React.Component {

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        const { onChangeRiskLevel } = this.props;
        const riskLevel = parseInt(event.target.value);
        onChangeRiskLevel(riskLevel);
    }

    render() {

        const { defaultRiskLevel, minRiskLevel, maxRiskLevel } = this.props;
        const options = [];
        for (let k = minRiskLevel; k <= maxRiskLevel; ++k) {
            options.push(
                <option key={k} value={k}>{k}</option>
            );
        }

        return (
            <div>
                Risk level:
                <select onChange={this.onChange} defaultValue={defaultRiskLevel}>
                    {options}
                </select>
            </div>
        );
    }
}

RiskLevelSelector.defaultProps = {
    defaultRiskLevel: config.defaults.riskLevel,
    minRiskLevel: config.minRiskLevel,
    maxRiskLevel: config.maxRiskLevel,
    onChangeRiskLevel: () => { }
};

RiskLevelSelector.propTypes = {
    defaultRiskLevel: PropTypes.number,
    minRiskLevel: PropTypes.number,
    maxRiskLevel: PropTypes.number,
    onChangeRiskLevel: PropTypes.func
};

export default RiskLevelSelector;
