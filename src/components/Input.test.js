import React from 'react';
import Input from './Input';

import Enzyme, { shallow, } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });


it('renders correctly', () => {
    const component = shallow(<Input value={10} />);
    expect(toJson(component)).toMatchSnapshot();
});

it('should call onChangeValue prop', () => {
    const onChangeValueMock = jest.fn();
    const event = {
        preventDefault() { },
        target: { value: 10 }
    };
    const component = shallow(<Input onChangeValue={onChangeValueMock} />);
    component.find('input').simulate('change', event);
    expect(onChangeValueMock).toBeCalledWith(10);
});


