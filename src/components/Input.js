import React from 'react';
import PropTypes from 'prop-types';

class Input extends React.Component {

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        const { onChangeValue } = this.props;
        const value = parseInt(event.target.value);
        onChangeValue(value);
    }

    render() {

        const { value, label } = this.props;

        return (
            <div>
                {label}
                <input onChange={this.onChange} defaultValue={value} />
            </div>
        );
    }
}

Input.defaultProps = {
    value: 0,
    label: "label: ",
    onChangeValue: () => { }
};

Input.propTypes = {
    value: PropTypes.number,
    label: PropTypes.string,
    onChangeValue: PropTypes.func
};

export default Input;
