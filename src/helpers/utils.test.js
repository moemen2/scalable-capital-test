import { roundToTwoDecimals, calculateTimeSeries } from './utils';

describe('roundToTwoDecimals', () => {
    it('rounds 10.5556789 to 10.56', () => {
        expect(roundToTwoDecimals(10.555)).toBe(10.56);
    });

    it('rounds 10.4446789 to 10.45', () => {
        expect(roundToTwoDecimals(10.4446789)).toEqual(10.44);
    });

    it('rounds 10.005 to 10.01', () => {
        expect(roundToTwoDecimals(10.005)).toEqual(10.01);
    });

    it('rounds 10.004 to 10.00', () => {
        expect(roundToTwoDecimals(10.004)).toEqual(10.00);
    });

    it('rounds 10.999 to 11.00', () => {
        expect(roundToTwoDecimals(10.999)).toEqual(11.00);
    });
});

describe('calculateTimeSeries', () => {
    it('returns allSeries', async () => {
        const series = calculateTimeSeries({ years: 1, mu: 0.0417, sigma: 0.0442, fee: 0.01, initialSum: 10000, monthlySum: 200 });
        expect(series).toMatchSnapshot();
    });
});


