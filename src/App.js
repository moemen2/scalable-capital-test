import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Menu from './components/Menu';
import RiskLevelSelector from './components/RiskLevelSelector';
import Input from './components/Input';
import { config } from './config';

const Table = lazy(() => import('./routes/Table'));
const Chart = lazy(() => import('./routes/Chart'));

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = config.defaults;
        this.onChangeRiskLevel = this.onChangeRiskLevel.bind(this);
        this.onChangeInitialSum = this.onChangeInitialSum.bind(this);
        this.onChangeYears = this.onChangeYears.bind(this);
        this.onChangeMonthlySum = this.onChangeMonthlySum.bind(this);
    }

    onChangeRiskLevel(riskLevel) {
        this.setState({ riskLevel });
    }
    onChangeInitialSum(initialSum) {
        this.setState({ initialSum });
    }
    onChangeYears(years) {
        this.setState({ years });
    }
    onChangeMonthlySum(monthlySum) {
        this.setState({ monthlySum });
    }

    render() {
        const { riskLevel, initialSum, years, monthlySum } = this.state;
        return (
            <Router>
                <div>
                    <Menu />
                    <RiskLevelSelector onChangeRiskLevel={this.onChangeRiskLevel} />
                    <Input onChangeValue={this.onChangeInitialSum} label="Initial Investment Sum: " value={initialSum} />
                    <Input onChangeValue={this.onChangeYears} label="Years of investment: " value={years} />
                    <Input onChangeValue={this.onChangeMonthlySum} label="Monthly Investment Sum: " value={monthlySum} />
                    <Suspense fallback={<div>Loading...</div>}>
                        <Switch>
                            <Route exact path="/" component={() => <Table riskLevel={riskLevel} initialSum={initialSum} years={years} monthlySum={monthlySum} />} />
                            <Route path="/table" component={() => <Table riskLevel={riskLevel} initialSum={initialSum} years={years} monthlySum={monthlySum} />} />
                            <Route path="/chart" component={() => <Chart riskLevel={riskLevel} initialSum={initialSum} years={years} monthlySum={monthlySum} />} />
                        </Switch>
                    </Suspense>
                </div>

            </Router>
        );
    }

}
