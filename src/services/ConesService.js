import { config } from '../config';
import axios from 'axios';


class ConesService {

    constructor() {
        this.apiUrl = config.apiUrl;
        this.apiPath = 'cones';
    }

    async getCones() {
        const result = await axios.get(this.apiUrl + this.apiPath);
        return result.data;
    }
}

export default ConesService;