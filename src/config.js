export const config = {
    defaults: {
        riskLevel: 10,
        initialSum: 10000,
        years: 10,
        monthlySum: 200
    },
    minRiskLevel: 3,
    maxRiskLevel: 25,
    fee: 0.01,
    apiUrl: 'http://localhost:3000/api/'
};