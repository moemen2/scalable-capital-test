import React from 'react';
import PropTypes from 'prop-types';
import { calculateTimeSeries, roundToTwoDecimals } from '../helpers/utils';
import { config } from '../config';
import ConesService from '../services/ConesService';

class Table extends React.Component {

    constructor() {
        super();
        this.conesService = new ConesService();
        this.state = {
            tableHeader: null,
            rows: null
        };
    }

    componentDidMount() {
        this.getTableData();
    }

    async getTableData(){
        const cones = await this.conesService.getCones();
        const { fee, riskLevel, initialSum, years, monthlySum } = this.props;
        const { mu, sigma } = cones.filter(cone => cone.riskLevel == riskLevel)[0];

        const timeSeries = calculateTimeSeries({
            mu,
            sigma,
            years,
            initialSum,
            monthlySum,
            fee
        });
        const tableData = {
            months: timeSeries.median.map((v, idx) => idx),
            dataGood: timeSeries.upper95.map(v => roundToTwoDecimals(v.y)),
            dataMedian: timeSeries.median.map(v => roundToTwoDecimals(v.y)),
            dataBad: timeSeries.lower05.map(v => roundToTwoDecimals(v.y))
        };

        const rows = tableData.months.map((entry, idx) => (
            <tr key={idx}>
                <td>{entry}</td>
                <td>{tableData.dataGood[idx]}</td>
                <td>{tableData.dataMedian[idx]}</td>
                <td>{tableData.dataBad[idx]}</td>
            </tr>
        ));

        const tableHeader = React.createElement('tr', {}, [
            React.createElement('th', { key: 'month' }, 'month'),
            React.createElement('th', { key: 'good' }, 'good'),
            React.createElement('th', { key: 'median' }, 'median'),
            React.createElement('th', { key: 'bad' }, 'bad')
        ]);
        this.setState({tableHeader: tableHeader, rows: rows});
    }

    render() {
        const tableHeader = this.state.tableHeader;
        const rows = this.state.rows;

        return (
            <table>
                <thead>
                    {tableHeader}
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        );
    }

}

Table.defaultProps = {
    ...config.defaults,
    fee: config.fee,
};

Table.propTypes = {
    riskLevel: PropTypes.number,
    fee: PropTypes.number,
    initialSum: PropTypes.number,
    years: PropTypes.number, 
    monthlySum: PropTypes.number
};

export default Table;
