import React from 'react';
import { Chart as ChartJs } from 'chart.js';
import { calculateTimeSeries, roundToTwoDecimals } from '../helpers/utils';
import PropTypes from 'prop-types';
import { config } from '../config';
import ConesService from '../services/ConesService';

class Chart extends React.Component {

    constructor() {
        super();
        this.conesService = new ConesService();
    }

    componentDidMount() {
        this.drawChart();
    }

    async drawChart() {
        const cones = await this.conesService.getCones();
        const { fee, riskLevel , initialSum, years, monthlySum } = this.props;
        const { mu, sigma } = cones.filter(cone => cone.riskLevel == riskLevel)[0];

        const timeSeries = calculateTimeSeries({
            mu,
            sigma,
            years,
            initialSum,
            monthlySum,
            fee
        });

        const labels = timeSeries.median.map((v, idx) => idx % 12 == 0 ? idx / 12 : '');
        const dataMedian = timeSeries.median.map(v => roundToTwoDecimals(v.y));
        const dataGood = timeSeries.upper95.map(v => roundToTwoDecimals(v.y));
        const dataBad = timeSeries.lower05.map(v => roundToTwoDecimals(v.y));

        const data = {
            datasets: [
                {
                    data: dataGood,
                    label: 'Good performance',
                    borderColor: 'rgba(100, 255, 100, 0.2)',
                    fill: false,
                    pointRadius: 0
                },
                {
                    data: dataMedian,
                    label: 'Median performance',
                    borderColor: 'rgba(100, 100, 100, 0.2)',
                    fill: false,
                    pointRadius: 0
                },
                {
                    data: dataBad,
                    label: 'Bad performance',
                    borderColor: 'rgba(255, 100, 100, 0.2)',
                    fill: false,
                    pointRadius: 0
                }
            ],
            labels
        };

        const options = {
            responsive: false,
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Years'
                    },
                    gridLines: {
                        drawOnChartArea: false
                    },
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Valuation (EUR)'
                    }
                }]
            }
        };

        const config = {
            type: 'line',
            data,
            options
        };

        const canvas = this.canvas;
        const context = canvas.getContext("2d");
        new ChartJs(context, config);
    }

    render() {
        return (
            <div>
                <canvas
                    ref={ref => this.canvas = ref}
                    width={600}
                    height={400}
                />
            </div>
        );
    }
}

Chart.defaultProps = {
    ...config.defaults,
    fee: config.fee,
};

Chart.propTypes = {
    riskLevel: PropTypes.number,
    fee: PropTypes.number,
    initialSum: PropTypes.number,
    years: PropTypes.number, 
    monthlySum: PropTypes.number
};

export default Chart;
